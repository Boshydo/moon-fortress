using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnergyGenerator : MonoBehaviour
{
    public int energy = 20;
    EconomyManager economyManager;
    MoneyUI moneyUI;
    public int cooldownTime = 4;
    public ParticleSystem explosionSpark;
    public TextMeshProUGUI energyText;
    void Start()
    {
        moneyUI = MoneyUI.Instance;
        economyManager = EconomyManager.Instance;
        StartCoroutine(nameof(AddEnergy));
       
    }
   public void EnergyGeneration()
    {
        Debug.Log(3);
        economyManager.AddMoney(energy);
        energyText.text = moneyUI.moneyText.ToString();
        if (explosionSpark.isPlaying) explosionSpark.Stop();
        explosionSpark.Play();
    }

    IEnumerator AddEnergy()
    {
        while (gameObject.activeSelf)
        {
            yield return new WaitForSeconds(cooldownTime);
          
            EnergyGeneration();
        }
}



}
