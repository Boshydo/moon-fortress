using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip sell;
    public AudioClip upgrade;
    public AudioClip turretPlacement;
    public AudioClip zoneEnter;
    public AudioClip defeat;
    public static SoundManager Instance;
    
    public void Awake()
    {
        if (Instance == null) Instance = this;
    }
    public void SellTurret()
    {
        AudioSource.PlayClipAtPoint(sell, GameObject.FindObjectOfType<CameraController>().transform.position);
    }
    public void UpgradeTurret()
    {
        AudioSource.PlayClipAtPoint(upgrade, GameObject.FindObjectOfType<CameraController>().transform.position);
    }
    public void TurretPlacement()
    {
        AudioSource.PlayClipAtPoint(turretPlacement, GameObject.FindObjectOfType<CameraController>().transform.position);
    }
    public void EnemyzoneEnter()
    {
        AudioSource.PlayClipAtPoint(zoneEnter, GameObject.FindObjectOfType<CameraController>().transform.position);
    }
    public void PlayerDefeat()
    {
        AudioSource.PlayClipAtPoint(defeat, GameObject.FindObjectOfType<CameraController>().transform.position);
    }
}
