using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	private Transform target;
	public float speed = 70f;
	public GameObject impactEffect;
	public float explosionRadius = 0f;
	public int damage = 10;


	public void Seek(Transform _target)
	{
		
		target = _target;
	}

	void Update()
	{
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        Vector3 dir = target.position - transform.position;
		float distanceThisFrame = speed * Time.deltaTime;

		if (dir.magnitude <= distanceThisFrame)
		{
			HitTarget();
			return;
		}
		transform.Translate(dir.normalized * distanceThisFrame, Space.World);
		transform.LookAt(target);
	}
	void HitTarget()
	{
        if (explosionRadius > 0f)
        { Explode(); }
        else 
		{ Damage(target); }
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation *Quaternion.Euler(90f,0,0));
        Destroy(effectIns, 0.5f);
		Destroy(gameObject);
    }
	void Damage(Transform enemy)
    {
		Bots bot = enemy.GetComponent<Bots>();
		if(bot !=null)
		bot.TakeDamage(damage);
		
    }
	void Explode()
    {
	 Collider[] colliders =	Physics.OverlapSphere(transform.position, explosionRadius);
		foreach (Collider collider in colliders){
			if(collider.tag == "Bots")	Damage(collider.transform);}
    }
    private void OnDrawGizmosSelected()
    {
		Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
