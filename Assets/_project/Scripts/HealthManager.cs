using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class HealthManager : MonoBehaviour
{
    public TextMeshProUGUI livesText;
    public static int lives;
    public int startLives = 5;
    void Start()
    {
        lives = startLives;
    }

    void Update()
    {
        livesText.text = lives.ToString();
        if (lives == 0) GameManager.Instance.GOCanvas();
    }

}
