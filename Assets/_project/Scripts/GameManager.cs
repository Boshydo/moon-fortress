using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverPanel;
    public GameObject pauseMenu;
    private bool gameEnded = false;
    public static bool isPaused = false;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI highScoreText;
    public TextMeshProUGUI scoreInGame;
    public static GameManager Instance;
    int score = 0;
    int highScore = 0;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
    private void Start()
    {  
        scoreText.text = scoreInGame.text = score.ToString();
        highScoreText.text = highScore.ToString();
    }
    public void AddPoints()
    {
        score += 1;
        scoreText.text = score.ToString();
        scoreInGame.text = scoreText.text;
        PlayerPrefs.SetInt("highScore", score);
    }
    
    void Update()
    {
        if (HealthManager.lives<= 0) EndGame();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused) ResumeGame();
            else PauseGame();
        }
    }
    void EndGame()
    {
        gameEnded = true;
    }
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void BackMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }
    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }
    public void PauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }
    public void GOCanvas()
    {
        if (HealthManager.lives == 0)
        {
            SoundManager.Instance.PlayerDefeat();
            gameEnded = true;
            gameOverPanel.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
