using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class MoneyUI : MonoBehaviour
{
    public static MoneyUI Instance;
    public TextMeshProUGUI moneyText;
    public void Awake()
    {
        if (Instance == null) Instance = this;
    }
    void Update()
    {
        moneyText.text = EconomyManager.money.ToString();
    }
}
