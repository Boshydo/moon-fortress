using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : TurretStats
{
	public GameObject bulletPrefab;
    private Transform target;
	private string enemyTag = "Bots";

	void Start()
	{
		InvokeRepeating("UpdateTarget", 0f, 0.5f);
	}
   void UpdateTarget()
    {
		GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
		float shortestDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;
		foreach(GameObject enemy in enemies)
        {
			float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                if (distanceToEnemy < shortestDistance)
            {
				shortestDistance = distanceToEnemy;
				nearestEnemy = enemy;
            } 
				if (nearestEnemy != null && shortestDistance <= attackRange)
            {
				target = nearestEnemy.transform;
            }
        }
    }
    private void Update()
    {
		if (target == null)
			return;
		Vector3 direction = target.position - transform.position;
		Quaternion lookRotation = Quaternion.LookRotation(direction);
		Vector3 rotation = lookRotation.eulerAngles;
		partToRotate.rotation = Quaternion.Euler(0f, rotation.y,0);

        if (cooldownTime <= 0f)
        {
			shoot();
			
			cooldownTime = 1f / fireRate;
        }
		cooldownTime -= Time.deltaTime;

    }
	void shoot()
    {
		GameObject bulletGO =(GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
		Bullet bullet = bulletGO.GetComponent<Bullet>();
		if(bullet != null)
        {
			bullet.Seek(target);
		}
	}

}



