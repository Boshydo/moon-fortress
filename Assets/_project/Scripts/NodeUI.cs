using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class NodeUI : MonoBehaviour
{
    private Node target;
    public Vector3 nodeOffset;
    public GameObject ui;
    public TextMeshProUGUI upgradeCost;
    public Button upgradeButton;
    public TextMeshProUGUI sellAmount;
    public void SetTarget(Node targetable)
    {
        target = targetable;
        transform.position = target.GetBuildPosition()+ nodeOffset;
        if (!target.isUpgraded)
        {
            upgradeCost.text = "$" + target.turretBluePrint.upgradeCost;
            upgradeButton.interactable = true;
        }
        else
        {
            upgradeButton.interactable = true;
            upgradeCost.text = "Maxed Out";
        }
        ui.SetActive(true);
        sellAmount.text = "$" + target.turretBluePrint.GetSellAmount();
        
    }
    public void Hide()
    {
        ui.SetActive(false);
    }
    public void Upgrade()
    {
        target.Upgrade();
        BuildManager.Instance.DeselectNode();
    }
    public void Sell()
    {
        target.SellTurret();
        BuildManager.Instance.DeselectNode();
    }
   
}
