using System.Collections;
using System.Collections.Generic;
using UnityEngine;


 [System.Serializable]
public class WaveAction
{
    public string waveName;
    public float delay;
    public GameObject enemyPrefab;
    public int spawnCount;
    
}
[System.Serializable]
public class Wave
{
    public string name;
    public List<WaveAction> actions;
}
