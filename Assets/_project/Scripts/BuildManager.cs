using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager Instance;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
    public GameObject machineGunTurret;
    public GameObject rocketLauncherTurret;
    public GameObject eneryPylonTurret;
    private TurretBluePrint turretToBuild;
    private Node selectedNode;
    public NodeUI nodeUI;
    public void SelectNode(Node node)
    {
        if (selectedNode == node)
        {
            DeselectNode();
            return;
        }
        selectedNode = node;
        turretToBuild = null;
        nodeUI.SetTarget(node);
    }
    public void DeselectNode()
    {
        selectedNode = null;
        nodeUI.Hide();

    }
    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return EconomyManager.money>= turretToBuild.cost; } }

    public void SetTurretToBuild(TurretBluePrint turret)
    {
        turretToBuild = turret;
        selectedNode = null;
        nodeUI.Hide();
    } 
    public TurretBluePrint GetTurretToBuild()
    {
        return turretToBuild;
    }
   
}
