using UnityEngine;

public class HealthBar : MonoBehaviour
{
    // The TextMesh Component
    TextMesh tm;

    // Use this for initialization
    void Start()
    {
        tm = GetComponent<TextMesh>();
    }

    void Update()
    {
        // Face the Camera
        transform.forward = Camera.main.transform.forward;
    }
    // Return the current Health by counting the '-'
    public int Current()
    {
        return tm.text.Length;
    }

    // Decrease the current Health by removing one '-'
    public void Decrease()
    {
        if (Current() > 1)
            tm.text = tm.text.Remove(tm.text.Length - 1);
        else
            Destroy(transform.parent.gameObject);
    }

}
