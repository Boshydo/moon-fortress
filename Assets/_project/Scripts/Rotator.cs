using UnityEngine;

public class Rotator : MonoBehaviour
{
	public Vector3 rotationSpeed;
	void Update()
	{
		transform.localEulerAngles += rotationSpeed;
	}
}
