using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveGenerator : MonoBehaviour
{
    public float difficultyFactor = 0.9f;
    public List<Wave> waves;
    private Wave CurrentWaves;
    WaveAction WaveAction;
    public Transform botSpawner;
    public Wave CurrentWave
    { get 
        { return CurrentWave; } 
    }
    private float DelayFactor = 1.0f;

    IEnumerator SpawnLoop()
    {
        DelayFactor = 1.0f;
        while (true)
        {
            foreach (Wave Wave in waves)
            {
                CurrentWaves = Wave;
                foreach (WaveAction enemy in Wave.actions)
                {
                    if (enemy.delay > 0)
                        yield return new WaitForSeconds(enemy.delay * DelayFactor);
                    
                    if (enemy.enemyPrefab != null && enemy.spawnCount > 0)
                    {
                        for (int i = 0; i < enemy.spawnCount; i++)
                        { 
                            GameObject enemyBot =  Instantiate(enemy.enemyPrefab, botSpawner.position, Quaternion.identity);
                            
                        }
                    }
                }
                yield return null;  
            }
            DelayFactor *= difficultyFactor;
            yield return null; 
        }
    }
    void Start()
    {
        StartCoroutine(SpawnLoop());
    }
}
