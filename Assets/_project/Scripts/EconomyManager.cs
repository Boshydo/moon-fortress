using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EconomyManager : MonoBehaviour
{
    public static EconomyManager Instance;
    public static int money;
    public int startMoney = 400;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
    void Start()
    {
        money = startMoney;
    }
    public void AddMoney(int amount) {
        money += amount;
    }
}
