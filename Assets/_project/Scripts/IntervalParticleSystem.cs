using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class IntervalParticleSystem : MonoBehaviour
{
	public ParticleSystem particleSystemToPlay;

	public float interval;

	protected DateTime NextPlayTime;

	void Start()
	{
		NextPlayTime = DateTime.Now.AddSeconds(interval);
	}

	void Update()
	{
		if (particleSystemToPlay != null && NextPlayTime <= DateTime.Now)
		{
			particleSystemToPlay.Play();
			NextPlayTime = DateTime.Now.AddSeconds(interval);
		}
	}
}

