using UnityEngine.SceneManagement;
using UnityEngine;

public class ManagerScene : MonoBehaviour
{   
    public GameObject instructionMenu;
    public GameObject mainMenuPanel;
    public void ActivatingInstructionMenu()
    {
        mainMenuPanel.SetActive(false);
        instructionMenu.SetActive(true);
    }
    public void ActivatingMainMenu()
    {
        mainMenuPanel.SetActive(true);
        instructionMenu.SetActive(false);
    }
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}

