using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Node : MonoBehaviour
{
    public GameObject turret;
    private Renderer rend;
    public Color hoverColor; 
    public Color notEnoughMoneyColor;
    public Color startColor;
    public Vector3 offset;
    BuildManager buildManager;
    public TurretBluePrint turretBluePrint;
    public bool isUpgraded = false;
    private void Start()
    {
        buildManager = BuildManager.Instance;
        rend = GetComponent<MeshRenderer>();
    }
    void BuildTurret(TurretBluePrint blueprint)
    {
        if (EconomyManager.money < blueprint.cost)
        {
            Debug.Log("INSUFFICIENT FUNDS");
            return;
        }
        EconomyManager.money -= blueprint.cost;

        GameObject _turret= (GameObject)Instantiate(blueprint.turretBlueprint, GetBuildPosition(), Quaternion.identity);
        turret = _turret;
        turretBluePrint = blueprint;
        Debug.Log("Turret Built! Money Left:" + EconomyManager.money);
    }
    private void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())  return;
        if (!buildManager.CanBuild)
        { return; }
        if (buildManager.HasMoney)
        {
            rend.material.color = hoverColor;
        }
        else
        {
            rend.material.color = notEnoughMoneyColor;
        }
    }
    private void OnMouseExit()
    {
        rend.material.color = startColor;
    }
    private void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;
      
        if  (turret != null){
            buildManager.SelectNode(this);
            return; }
        if (!buildManager.CanBuild)
        { return; }
        BuildTurret(buildManager.GetTurretToBuild());
    }
    public void SellTurret()
    {
        SoundManager.Instance.SellTurret();
        EconomyManager.money += turretBluePrint.GetSellAmount();
        Destroy(turret);
        turretBluePrint = null;
    }
    public Vector3 GetBuildPosition()
    {
        SoundManager.Instance.TurretPlacement();
        return transform.position + offset ;
    }
    public void Upgrade()
    {
        if (EconomyManager.money < turretBluePrint.upgradeCost)
        {
            Debug.Log("INSUFFICIENT FUNDS TO UPGRADE");
            return;
        }
        SoundManager.Instance.UpgradeTurret();
        EconomyManager.money -= turretBluePrint.upgradeCost;
        Destroy(turret);

        GameObject _turret = (GameObject)Instantiate(turretBluePrint.upgradedTurret, GetBuildPosition(), Quaternion.identity);
        turret = _turret;
        Debug.Log("turret Upgraded");
        isUpgraded = true;
    }
  
}