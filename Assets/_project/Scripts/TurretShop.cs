using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretShop : MonoBehaviour
{
    BuildManager buildManager;
    public TurretBluePrint machineGunTurret;
    public TurretBluePrint rocketLauncher;
    public TurretBluePrint energyPylon;
    private void Start()
    {
        buildManager = BuildManager.Instance;
    }
    public void PurchaseMachineGunTurret()
    {
        Debug.Log("Machine Gun Purchased");
        buildManager.SetTurretToBuild(machineGunTurret);
    }
    public void PurchaseRocketLauncherTurret()
    {
        Debug.Log("Rocket Launcher Purchased");
        buildManager.SetTurretToBuild(rocketLauncher);
    }
    public void PurchaseEnergyPylonTurret()
    {
        Debug.Log("Enery Pylon Purchased");
        buildManager.SetTurretToBuild(energyPylon);
    }
}
