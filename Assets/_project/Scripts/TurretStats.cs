using UnityEngine;

public class TurretStats : MonoBehaviour
{
    
    public float attackRange = 15;
    public float cooldownTime;
    public float fireRate;
    public Transform partToRotate;
    public float turnSpeed = 10f;
    public Transform firePoint;
    protected Bots targetEnemy;
 

   
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
}
