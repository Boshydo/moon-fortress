using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TurretBluePrint 
{
    public GameObject turretBlueprint;
    public int cost;

    public GameObject upgradedTurret;
    public int upgradeCost;

    public int GetSellAmount()
    {
        return cost / 2;
    }
}
