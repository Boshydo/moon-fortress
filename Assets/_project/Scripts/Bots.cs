using UnityEngine;
using UnityEngine.AI;

public class Bots : MonoBehaviour
{
    public int health = 100;
    public AudioClip explosion;
    void Start()
    {
        
        GameObject castle = GameObject.Find("baseTarget");
        if (castle) GetComponent<NavMeshAgent>().destination = castle.transform.position;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Core")
        {
            SoundManager.Instance.EnemyzoneEnter();
            HealthManager.lives--;
            Destroy(gameObject);
        }
    }
    public void TakeDamage(int amount)
    {
        health -= amount;

        if (health <= 0)
        {
            AudioSource.PlayClipAtPoint(explosion, GameObject.FindObjectOfType<CameraController>().transform.position);
            Debug.Log(explosion);
            Die();
            GameManager.Instance.AddPoints();
            Debug.Log("score");
        }
    }
    private void Die()
    {
        
        Destroy(gameObject);
        
    }
}
